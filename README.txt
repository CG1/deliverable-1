README

CPSC 219, Group 1
Deliverable 1

Authors: Bayo Omole, Chris Hawkins, Hannah Sun, Parvathi Nair
Delivered date: 1 February, 2013
Program: Snake Game Password


Getting Code

Visit https://bitbucket.org/CG1/deliverable-1
On the right half of the screen, the specifications of the repository is shown,
i.e. owner, type, last modified date, creation date, and size. Beside the size
is a hyperlink that allows for download of the entire repository. Click the
hyperlink and download the zip file into an appropriate location on your local
computer. You will now have access to a copy of all the code and documents
found in the respository


Downloaded Files

1. README.txt: Copy of this README file
2. Design Document 1.doc: Design document for Deliverable 1
3. SnakeGamev1.java: A rudimentary java program that will draw a line on a 
   canvas
4. SnakeGamev1.html: The HTML file required to run the SnakeGamev1.java applet
5. Apple.java: A java program that specifies the shape, size, and color of an 
   apple for use on a canvas
6. Orange.java: A java program that specifies the shape, size, and color of an 
   orange for use on a canvas
7. RandomPlace.java: A java program that randomly generates x and y coordinates 
   on the canvas and places an apple and an orange on it
8. RandomPlace.html: The HTML file required to run the RandomPlace java applet
9. userPick.java: A java program that will allow user to enter what fruit they want
   displayed on the canvas and where it needs to be displayed
10. nonUserPick.java: A java program that will run if the user does not pick location
11. UserPickTest.java: A test java program to run userPick.java
12. UserPickTest.html: The HTML file required to run the UserPickTest.java applet


Opening Files

Design Document 1.doc is compatible with Word 97 upwards. In order to access the
java code, an appropriate editor must be installed on your computer 
(emacs, Notepad++, gedit). The HTML files can also be opened in one of these editors.

	
Compiling Code to Run Program

Open Terminal on Mac or Command Prompt on Windows.
Go into the directory with the downloaded files by typing <cd path/to/directory>
Enter <javac *.java> to compile all java files.
In order to run the code, enter <appletviewer UserPickTest.html>


Understanding the Repository

Visit https://bitbucket.org/CG1/deliverable-1
Click on the 'Commits' tab to view the page that shows where branches were made, 
when the branches were merged, and who created each new addition to the repository. 
The tree diagram shows the progression of the repository from the beginning 
(lowest point) to the end (highest point). Each team member added some code 
throughout the process, either changing existing programs or implementing new features.


Credits

Chris: SnakeGamev1.java
Bayo: SnakeGamev1.java
Hannah: Apple.java, Orange.java, Randomplace.java, userPick.java, nonUserPick.java
Parvathi: README.txt, Design Document 1.doc, SnakeGamev1.java, UserPickTest.java





