import java.awt.*;

public class Snake
{

	private static int DIAMETER = 17;
	private static int START_X = 100;
	private static int START_Y = 100;
	private static int NUM_SEGMENTS = 2;

	public void startPosition (Graphics canvas)
	{
		canvas.setColor(Color.red);
		canvas.fillOval(START_X, START_Y, DIAMETER,DIAMETER);
		canvas.setColor(Color.green);
		canvas.fillOval(START_X - 16, START_Y, DIAMETER, DIAMETER);
	}
}
