import java.awt.Graphics;
import java.util.*;
import java.lang.String;
/**this is for taking user input of the eatable
  *@authur Hannah Sun
	*Comments: Parvathi
  *Date modified: 01/02/2013
*/
public class userPick
{
  private boolean Apple=false;
  private boolean Orange=false;
  private int x=100;
  private int y=100;
  public String pick;
  
	// User selects fruit as well as location of where it must be set on the canvas
  public void input()
  {
    Scanner keyboard = new Scanner(System.in);
    System.out.println("Please choose a eatable:(apple or orange)");
    pick=keyboard.nextLine();
    System.out.println("please enter a x-value(50-450)");
    setX(keyboard.nextInt());
    System.out.println("please enter a y-value(50-450)");
    setY(keyboard.nextInt());
		// Comparing string entered by user with 'apple' or 'orange' or anything we
		// decide to make the objects in the future.
		compare();
  }
  
	// if user entered 'apple', boolean for apple must be true
	// if user entered 'orange', boolean for orange must be true
	// REQUIRES ERROR CHECKING (for later!)
  private void compare()
  {
    String a="apple";
		String o = "orange";
    if (pick.equalsIgnoreCase(a)){
      setApple(true);
    }
		else if (pick.equalsIgnoreCase(o)){
      setOrange(true);
    }
  }
  
	// Modifier
  private void setApple(boolean pickApple)
  {
    Apple=pickApple;
  }
  
	// Modifier
  private void setOrange(boolean pickOrange)
  {
    Orange=pickOrange;
  }
  
	/** User entered number. Modifier
		* @param aX x-coordinate of the fruit
		* Will work in range of 50-450. Else default 100
	*/
  private void setX(int aX)
  {
    if (aX>=50 && aX<=450){
      x=aX;
    }
  }

	/** User entered number. Modifier
		* @param aY y-coordinate of the fruit
		* Will work in range of 50-450. Else default 100
	*/
  private void setY(int aY)
  {
    if (aY>=50 && aY<=450){
      y=aY;
    }
  }

	/** Accessor. 
		* 
	*/
  public void drawPick(Graphics canvas)
  {

		// If user picked apple, then draw apple in set location
		// also draw a random orange
    if (Apple){
      Apple apple2= new Apple();
      apple2.setAppleX(x);
      apple2.setAppleY(y);
      apple2.draw(canvas);
      nonUserPick notPick = new nonUserPick();
      notPick.setNotApple(true);
      notPick.drawRandom(canvas);
    }
		// If user picked orange, then draw orange in set location
		// also draw a random apple
    else if (Orange){
      Orange orange1 = new Orange();
      orange1.setOrangeX(x);
      orange1.setOrangeY(y);
      orange1.draw(canvas);
      nonUserPick notPick = new nonUserPick();
      notPick.setNotOrange(true);
      notPick.drawRandom(canvas);
    }
		// If user did not type in a valid string, draw an apple and orange in
		// random locations
		else {
			nonUserPick notPick = new nonUserPick();
			notPick.drawRandom(canvas);
		}
    
  }
}
