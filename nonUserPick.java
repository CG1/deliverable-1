import java.awt.Graphics;
/**this is for create a random eatable for the shape not been picked
  *@authur Hannah Sun
  *Date modified: 01/02/2013
*/
public class nonUserPick
{
  private boolean PApple=false;
  private boolean POrange=false;

	// Modifier.
  public void setNotApple(boolean nApple)
  {
    PApple=nApple;
  }

	// Modifier.
  public void setNotOrange(boolean nOrange)
  {
    POrange=nOrange;
  }

	// Accessor
  public void drawRandom(Graphics canvas)
  {
    RandomPlace pick = new RandomPlace();
		// If apple was picked by the user, draw a random orange
    if (PApple){
      pick.OPaint(canvas);
    }

		// If orange was picked by user, draw a random apple
    else if (POrange){
      pick.APaint(canvas);
    }

		// If neither eatable was picked, draw random for both
		else
		{
			pick.APaint(canvas);	
			pick.OPaint(canvas);
		}
  }
}
