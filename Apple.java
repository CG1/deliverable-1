import java.awt.*;
/** Apple shape of the eatable
  * @author Hannah Sun
  * Date modified: 30/01/2013
*/
public class Apple
{
  private int x;
  private int y;
  private int size=15;


  public void setAppleX(int appleX)
  {
    x=appleX;
  }

  public void setAppleY(int appleY)
  {
    y=appleY;
  }
  
  public void draw(Graphics canvas){
    canvas.setColor(Color.darkGray);
    canvas.fillRect(x+6,y-3,size-13,size-5);
    canvas.setColor(Color.red);
    canvas.fillOval(x,y,size,size);
    canvas.setColor(Color.green);
    canvas.fillOval(x+7,y-4,size-9,size-11);
  }
}
