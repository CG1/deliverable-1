import java.awt.*;
import javax.swing.JApplet;

/** First draft of the snake game!
  * @author Bayo Omole
  * Date modified: January 30, 2013
  * Starting off with some variables and drawing one line on a canvas.
  * Use html file. On Terminal or Command Prompt, compile and then type 'appletviewer SnakeGamev1.html'
  */

public class SnakeGamev1 extends JApplet
{
    private static final int X_START = 50;
    private static final int X_END = 400;
   	private static final int Y_START = 50;
    private static final int Y_END = 400;
    private static final int START_LENGTH = 20;
    private static final int MAX_LENGTH = 80;

   	public void paint (Graphics canvas)
    {

       // not required yet, but useful for incrementing in for loop below
       int increment = 10;
     
	
		// Initialize starting message
		String Start = "...AND START!!!!";
		Font small = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metr = this.getFontMetrics(small);	

		// Initialize end message
    String End = "Game Over";
		
		// Get timer!
		canvas.setColor(Color.green);
		canvas.setFont(small);
		canvas.drawString(Start, 100,100);

		// Clears canvas		
		// canvas.clearRect(0, 0, 500, 500);

		canvas.setColor(Color.green);
		canvas.setFont(small);
		canvas.drawString(End, 200, 200);
     }
}


