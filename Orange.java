import java.awt.*;
/** Orange shape of the eatable
  * @author Hannah Sun
  * Date modified: 30/01/2013
*/
public class Orange{
  private int x;
  private int y;
  private int size=15;

  public void setOrangeX(int orangeX){
    x=orangeX;
  }

  public void setOrangeY(int orangeY){
    y=orangeY;
  }
  public void draw(Graphics canvas)
  {
    canvas.setColor(Color.orange);
    canvas.fillOval(x,y,size,size);
    canvas.setColor(Color.green);
    canvas.fillOval(x+6,y-1,size-10,size-11);
  }
}
