import java.util.*;
import java.awt.*;
import javax.swing.JApplet;

/** @author Hannah Sun
	* Modified: 01/02/2013
	* Generates random positions for eatables
*/
public class RandomPlace extends JApplet
{
  private static int MAX=400;

	// Generates random number. Mutator
  private int generate()
  {
    Random generator = new Random(); 
    return generator.nextInt(MAX+50);
  }
  
	// Accessor. Draw apple in random location
  public void APaint(Graphics canvas)
  {
    Apple apple1= new Apple();
    apple1.setAppleX(generate());
    apple1.setAppleY(generate());
    apple1.draw(canvas);
  }
  	
	// Accessor. Draw orange in random location
  public void OPaint(Graphics canvas)
  {
    Orange orange1 = new Orange();
    orange1.setOrangeX(generate());
    orange1.setOrangeY(generate());
    orange1.draw(canvas);
  }
  
}
